import sqlalchemy as db
import datetime
engine = db.create_engine('postgresql+psycopg2://postgres:1234@localhost:5432/postgres')

connection = engine.connect()

metadata = db.MetaData()
dats = db.Table('healthdb', metadata, autoload=True, autoload_with=engine)

print(dats.columns.keys())

def input_db(tipe, size):
    if tipe == 'cpu_memory':
        qry = db.insert(dats).values(type=tipe,date=datetime.datetime.now(), percent=size)
    else:
        qry = db.insert(dats).values(type=tipe,date=datetime.datetime.now(), memory=size)
    res = connection.execute(qry)
    print(res)


# qry = db.insert(dats)
# value = [{'type': 'dummy', 'date': datetime.datetime, 'memory': 50000}]

# qry = db.insert(dats).values(type='dummy',date=datetime.datetime.now(), memory=50000)
# result = connection.execute(qry)
# # result = connection.execute(qry, value)
# print(result)

# d1 = db.select([dats]).where(dats.columns.memory)
import psutil
from apscheduler.schedulers.blocking import BlockingScheduler #BackgroundScheduler, BlockingScheduler
from flask import Flask
from cel_module import cpu_buffer
from multiprocessing import Process, Queue
import time
from db_setting import input_db
import argparse

# durasi dalam menit
dur1 = 5
dur2 = 15

print(psutil.cpu_percent())
print("###")
mem_tot = dict(psutil.virtual_memory()._asdict())
print(mem_tot)
print("###")

def cpu_info():
    print('cpu memory usage:', psutil.cpu_percent())


def mem_info():
    while True:
        mem_used = int(mem_tot['used']/100)
        print("memori used: ", mem_used)
        input_db('Memory Usage', mem_used)
        time.sleep(dur1*60)


# schd = BackgroundScheduler(daemon="True")
app = Flask(__name__)
schd = BlockingScheduler()


 

# isflask = True
# isCelery = True
# isMultipros = True

parser = argparse.ArgumentParser(description='choose between celery and multipros')
parser.add_argument('--celery', action='store_true', help='Execute celeri task')
parser.add_argument('--multipros', action='store_true', help='Execute multiprocessing task')
ars = parser.parse_args()


# if isCelery:
if ars.celery:
    schd.add_job(cpu_buffer, 'interval', minutes=dur2)
    schd.start()
    # app.run('0.0.0.0',port=2222)
if ars.multipros:
    p = Process(target = mem_info)
    p.start()


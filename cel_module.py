from celery import Celery
from psutil import virtual_memory, cpu_percent
from db_setting import input_db

app = Celery('cel_module',
             broker='amqp://irsyad:123@localhost/irsyad_vhost',
             backend='rpc://')#,
            #  include=['test_celery.tasks'])

@app.task
def cpu_infos(a):
    print('cpu memory usage:', a)
    # input_db("cpu_memory", cpu_percent())
    return a

def cpu_buffer():
    cp = cpu_percent()
    # print('trig cpu', cp)
    tsk =cpu_infos.delay(cp)
    while not tsk.ready():
        pass
    print(tsk.ready(), tsk.result)
    input_db("cpu_memory", tsk.result)
    # print(tsk.result)

